<?php
	//setting MySql Connection
	$db_host = "localhost"; //hostname
	$db_user = "root"; // username
	$db_password = "amit@043015921"; // password
	$db_name = "hukksApp"; //database name
	
	// Create connection
	$conn = new mysqli($db_host, $db_user, $db_password, $db_name);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	define('app_key', '123456');

	$requestMethod = $_SERVER['REQUEST_METHOD'];

	$appKey = $_GET['app_key'];

	switch ($requestMethod) {
		case 'GET':
			if(isset($_GET['id']) && $_GET['id'] != '')
			{
				getOrderById($conn, $_GET['id'], $appKey);
			}
			else
			{
				$fromDate = null;
				$toDate = null;
				if(isset($_GET['from_date']) && $_GET['from_date'] != '')
				{
					$fromDate = $_GET['from_date'];
				}

				if(isset($_GET['to_date']) && $_GET['to_date'] != '')
				{
					$toDate = $_GET['to_date'];
				}

				allOrders($conn, $appKey, $fromDate, $toDate);
			}
			break;

		case 'POST':
			if(isset($_GET['id']) && $_GET['id'] != '')
			{
				updateOrderById($conn, $_GET['id'], $appKey);
			}
			else
			{
				createOrder($conn, $appKey);
			}
			break;
		
		default:
			badRequest();
			break;
	}


	//Get All Order
	function allOrders($conn, $appKey, $fromDate, $toDate)
	{
		if($appKey == app_key)
		{
			if($fromDate == null && $toDate == null)
			{
				$query = $conn->prepare("select * from orders order by created_at desc");
			}
			else
			{
				$query = $conn->prepare("select * from orders where created_at between ? and ? order by created_at desc");
				$query->bind_param('ss', $fromDate, $toDate);
			}

			$query->execute();
			$result = $query->store_result();
			$num_of_rows = $query->num_rows;
			$query->bind_result($id, $name, $mobile,$device_id, $delivery_date_time, $status, $created_at, $updated_at);			

			if ($num_of_rows > 0) 
			{
			    // output data of each row
			    while($query->fetch()) 
			    {
			        $orders[] = array(
			        					'id' => $id,
			        					'name' => $name,
			        					'mobile' => $mobile,
			        					'device_id' => $device_id,
			        					'delivery_date_time' => $delivery_date_time,
			        					'status' => $status,
			        					'created_at' => $created_at,
			        					'updated_at' => $updated_at	
			        				);
			    }

			    $data = array("code" => "200", "orders" => $orders);
			} 
			else 
			{
			    $data = array("code" => "204", "message" => "no orders available in database");
			}

			// header("HTTP/1.0 204 No Content");
			header("Content-type: application/json");
			echo json_encode($data);
		}
		else
		{
			header("Content-type: application/json");
			$data = array("code" => "203", "message" => "Non-Authoritative Information");
			echo json_encode($data);
		}		
	}

	//Store order
	function createOrder($conn, $appKey)
	{
		if($appKey == app_key)
		{
			$name = (isset($_POST['name']))?$_POST['name']:'';
			$mobile = (isset($_POST['mobile']))?$_POST['mobile']:'';
			$device_id = (isset($_POST['device_id']))?$_POST['device_id']:'';
			$created_at = date('Y-m-d H:i:s');
			$updated_at = date('Y-m-d H:i:s');
			$delivery_date_time = date('Y-m-d H:i:s');
			$status = '0';

			if($name != '' && $mobile != '' && $device_id != '')
			{
				$query = $conn->prepare("INSERT INTO orders (name, mobile, device_id, delivery_date_time, status, created_at, updated_at) VALUES (?,?,?,?,?,?,?)");

				$query->bind_param('sssssss', $name, $mobile, $device_id, $delivery_date_time, $status, $created_at, $updated_at);

				$result = $query->execute();
				$last_id = $conn->insert_id;
				$query->close();

				if($last_id)
				{
					$data = array("code" => "201", "message" => "Order created.");
				}
			}
			else
			{
				$data = array("code" => "400","message" => "Empty value");
			}

			header("Content-type: application/json");
			echo json_encode($data);	
		}
		else
		{
			header("Content-type: application/json");
			$data = array("code" => "203", "message" => "Non-Authoritative Information");
			echo json_encode($data);
		}	
	}

	//Get Order By Id
	function getOrderById($conn, $id, $appKey)
	{
		if($appKey == app_key)
		{
			$query = $conn->prepare("select * from orders where id = ?");
			$query->bind_param('i', $id);
			$query->execute();
			$result = $query->store_result();
			$num_of_rows = $query->num_rows;
			$query->bind_result($id, $name, $mobile,$device_id, $delivery_date_time, $status, $created_at, $updated_at);

			$order = array();

			if($num_of_rows == 1)
			{
				while ($query->fetch()) 
				{
	    			$order['id'] = $id;
					$order['name'] = $name;
					$order['mobile'] = $mobile;
					$order['device_id'] = $device_id;
					$order['delivery_date_time'] = $delivery_date_time;
					$order['status'] = $status;
					$order['created_at'] = $created_at;
					$order['updated_at'] = $updated_at;      
				}

				$data = array("code" => "200", "order" => $order);
			}
			else
		    {
		    	$data = array("code" => "204", "message" => "Order #$id not found");
		    }		    

			header("Content-type: application/json");
			echo json_encode($data);	
		}
		else
		{
			header("Content-type: application/json");
			$data = array("code" => "203", "message" => "Non-Authoritative Information");
			echo json_encode($data);
		}	
	}

	//Update Order By Id	
	function updateOrderById($conn, $id, $appKey)
	{
		if($appKey == app_key)
		{
			$query = "select * from orders where id = '".$id."'";
			$result = $conn->query($query);
			$order = $result->fetch_assoc();

			if($order)
			{
				$delivery_date_time = (isset($_POST['delivery_date_time']))?$_POST['delivery_date_time']:$order['delivery_date_time'];
				$status = (isset($_POST['status']))?$_POST['status']:$order['status'];
				$updated_at = date('Y-m-d H:i:s');

				$query = $conn->prepare("UPDATE orders SET delivery_date_time = ?, status = ?, updated_at = ? WHERE id = ?");

				$query->bind_param('sssi', $delivery_date_time, $status, $updated_at, $id);
				$result = $query->execute();
				$query->close();

				if($result)
				{
					$data = array("code" => "201", "message" => "Order updated.");
				}
			}
			else
			{
				$data = array("code" => "204", "message" => "Order #$id not found");
			}

			header("Content-type: application/json");
			echo json_encode($data);			
		}
		else
		{
			header("Content-type: application/json");
			$data = array("code" => "203", "message" => "Non-Authoritative Information");
			echo json_encode($data);
		}		
	}	

	//Wrong Request
	function badRequest()
	{
		header("Content-type: application/json");
		$data = array("code" => "400", "message" => "Bad request");
		echo json_encode($data);
	}
?>