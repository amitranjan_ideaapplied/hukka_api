GET: ALL Orders
http://localhost/ideaappliedtechnologies/hukka-api/api.php?app_key=123456

Response:

{"code":"200","orders":[{"id":13,"name":"rahul","mobile":"3432432345","device_id":"dskjvihsdvj","delivery_date_time":"2016-08-03 11:59:19","status":"0","created_at":"2016-08-03 11:59:19","updated_at":"2016-08-03 11:59:19"},{"id":12,"name":"rahul","mobile":"3432432345","device_id":"dskjvihsdvj","delivery_date_time":"2016-08-03 11:02:38","status":"0","created_at":"2016-08-03 11:02:38","updated_at":"2016-08-03 11:02:38"}]}

Response: no records
{"code":"204","message":"no orders available in database"}

--------------------------------------------------------------------------------------------------------------------------------------------------

GET: Order by Id
http://localhost/ideaappliedtechnologies/hukka-api/api.php?app_key=123456&id=1

Response:
{"code":"200","order":{"id":1,"name":"Amit Ranjan","mobile":"9901032299","device_id":"sdkjfah","delivery_date_time":"2016-08-03 10:53:25","status":"0","created_at":"2016-08-01 10:53:25","updated_at":"2016-08-03 10:53:25"}}

Response: no records
{"code":"204","message":"Order #111 not found"}

--------------------------------------------------------------------------------------------------------------------------------------------------

GET: Order between to dates
http://localhost/ideaappliedtechnologies/hukka-api/api.php?app_key=123456&from_date=2016-08-02&to_date=2016-08-03

Response: success
{"code":"200","orders":[{"id":3,"name":"rahul1","mobile":"3432432345","device_id":"dskjvihsdvj","delivery_date_time":"2016-08-03 10:58:01","status":"0","created_at":"2016-08-02 10:58:01","updated_at":"2016-08-03 10:58:01"},{"id":4,"name":"rahul2","mobile":"3432432345","device_id":"dskjvihsdvj","delivery_date_time":"2016-08-03 10:58:01","status":"0","created_at":"2016-08-02 10:58:01","updated_at":"2016-08-03 10:58:01"}]}

Response: no records
{"code":"204","message":"no orders available in database"}

--------------------------------------------------------------------------------------------------------------------------------------------------

POST: Create Order
http://localhost/ideaappliedtechnologies/hukka-api/api.php?app_key=123456

need data
1. name
2. device_id
3. mobile

Response: success
{"code":"201","message":"Order created."}

Response: empty value if data not provided
{"code":"400","message":"Empty value"}

--------------------------------------------------------------------------------------------------------------------------------------------------

POST: update Order
http://localhost/ideaappliedtechnologies/hukka-api/api.php?app_key=123456&id=1

need data
1. delivery_date_time
2. status

Response: Success
{"code":"201","message":"Order updated."}

Response: Failed
{"code":"204","message":"Order #1 not found"}

--------------------------------------------------------------------------------------------------------------------------------------------------

Response: if you will not pass app_key in url 
{"code":"203","message":"Non-Authoritative Information"}

Database: hukksApp
Table:

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `delivery_date_time` datetime DEFAULT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;







